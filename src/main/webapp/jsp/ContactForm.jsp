<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please create Contact
    </div>
    <form method="post" action="/contact/add">
        <label for="first-name">First Name
            <input class="input-field" type="text" id="first-name" name="first-name">
        </label>
        <label for="last-name">Last Name
            <input class="input-field" type="text" id="last-name" name="last-name">
        </label>
        <label for="note">Note
            <input class="input-field" type="text" id="note" name="note">
        </label>
        <label for="birthday">Birthday
            <input class="input-field" type="date" id="birthday" name="birthday">
        </label>
        <input type="submit" value="Add contact">
    </form>
</div>
</body>
</html>