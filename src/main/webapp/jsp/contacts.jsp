<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Contacts</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        All Contacts
    </div>
    <div class="link-2">
        <a href="contact/add">
            <span class="thick">Add</span><span class="thin"> new Contact</span>
        </a>
    </div>
    <table>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phones</th>
            <th>Birthday</th>
            <th>Notes</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${contactsFromServer}" var="contacts">
            <tr>
                <td>${contacts.contactId}</td>
                <td>${contacts.firstName}</td>
                <td>${contacts.lastName}</td>
                <td>${contacts.phones}</td>
                <td>${contacts.birthday}</td>
                <td>${contacts.note}</td>
                <td>
                <div class="link-2">
                   <a href="/contact/update?id=<c:out value='${contacts.contactId}' />">
                      <span class="thick">E</span><span class="thin">dit</span>
                   </a>
                   <a href="/contact/delete?id=<c:out value='${contacts.contactId}' />">
                      <span class="thick">D</span><span class="thin">elete</span>
                   </a>
                </div>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>