package edu.j4edu.dao;

import edu.j4edu.model.Contact;

import java.util.List;

public interface ContactsDAO extends CrudDao<Contact> {
  List<Contact> findByFirstName(String firstName);
}
