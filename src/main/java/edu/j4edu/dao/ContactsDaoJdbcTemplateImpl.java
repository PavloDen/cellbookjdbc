package edu.j4edu.dao;

import edu.j4edu.model.Contact;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.*;

public class ContactsDaoJdbcTemplateImpl implements ContactsDAO {

  private JdbcTemplate jdbcTemplate;
  private SimpleJdbcInsert insertContact;

  private final String SQL_SELECT_ALL_CONTACTS = "SELECT * FROM contact ORDER BY contactid ASC;";
  private final String SQL_SELECT_CONTACT_BY_ID = "SELECT * FROM contact WHERE contactid = ?;";

  private final String SQL_UPDATE_CONTACT =
      "UPDATE public.contact SET firstname = ?, lastname = ?, notes = ?, birthday = ? WHERE contactid = ?;";
  private final String SQL_DELETE_CONTACT_BY_ID = "DELETE FROM contact WHERE contactid = ?;";

  private Map<Integer, Contact> contactsMap = new HashMap<>();

  public ContactsDaoJdbcTemplateImpl(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.insertContact =
        new SimpleJdbcInsert(dataSource)
            .withTableName("contact")
            .usingGeneratedKeyColumns("contactid");
  }

  private RowMapper<Contact> contactRowMapper =
      (ResultSet resultSet, int i) -> {
        Integer id = resultSet.getInt("contactid");
        if (!contactsMap.containsKey(id)) {
          String firstName = resultSet.getString("firstname");
          String lastName = resultSet.getString("lastname");
          String note = resultSet.getString("notes");
          Date birthday = resultSet.getDate("birthday");
          Contact contact = new Contact(id, firstName, lastName, note, birthday);
          contactsMap.put(id, contact);
        }
        return contactsMap.get(id);
      };

  @Override
  public List<Contact> findByFirstName(String firstName) {
    return null;
  }

  @Override
  public Optional<Contact> findById(Integer id) {
    Contact contact = new Contact();
    contact = jdbcTemplate.queryForObject(SQL_SELECT_CONTACT_BY_ID, contactRowMapper, id);
    return Optional.ofNullable(contact);
  }

  @Override
  public void save(Contact model) {
    Map<String, Object> parameters = new HashMap<String, Object>(4);
    parameters.put("firstname", model.getFirstName());
    parameters.put("lastname", model.getLastName());
    parameters.put("notes", model.getNote());
    parameters.put("birthday", model.getBirthday());
    insertContact.execute(parameters);
  }

  @Override
  public void update(Contact model) {
    jdbcTemplate.update(
        SQL_UPDATE_CONTACT,
        model.getFirstName(),
        model.getLastName(),
        model.getNote(),
        model.getBirthday(),
        model.getContactId());
  }

  @Override
  public void deleteById(Integer id) {
    Object[] args = new Object[] {id};
    jdbcTemplate.update(SQL_DELETE_CONTACT_BY_ID, args);
  }

  @Override
  public List<Contact> findAll() {
    return jdbcTemplate.query(SQL_SELECT_ALL_CONTACTS, contactRowMapper);
  }
}
