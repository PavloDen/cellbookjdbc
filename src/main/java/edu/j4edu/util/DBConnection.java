package edu.j4edu.util;

import edu.j4edu.dao.ContactsDaoJdbcTemplateImpl;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class DBConnection {

  public static ContactsDaoJdbcTemplateImpl createConnection() {

    Properties properties = new Properties();
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    try {
      properties.load(new FileReader("src/main/resources/db.properties"));
      String dbUrl = properties.getProperty("jdbc.url");
      String dbUsername = properties.getProperty("jdbc.username");
      String dbPassword = properties.getProperty("jdbc.password");
      String driverClassName = properties.getProperty("jdbc.driver");
      dataSource.setUsername(dbUsername);
      dataSource.setPassword(dbPassword);
      dataSource.setUrl(dbUrl);
      dataSource.setDriverClassName(driverClassName);
      return new ContactsDaoJdbcTemplateImpl(dataSource);
    } catch (FileNotFoundException e) {
      throw new IllegalArgumentException(e);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
