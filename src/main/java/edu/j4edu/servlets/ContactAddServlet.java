package edu.j4edu.servlets;

import edu.j4edu.dao.ContactsDAO;
import edu.j4edu.model.Contact;
import edu.j4edu.util.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/contact/add")
public class ContactAddServlet extends HttpServlet {
  private ContactsDAO contactsDAO;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request
        .getServletContext()
        .getRequestDispatcher("/jsp/ContactForm.jsp")
        .forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    contactsDAO = DBConnection.createConnection();
    String fName = req.getParameter("first-name");
    String lName = req.getParameter("last-name");
    String note = req.getParameter("note");
    Date birthday = null;
    try {
      birthday = new SimpleDateFormat("yyy-MM-dd").parse(req.getParameter("birthday"));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    Contact contact = new Contact(fName, lName, note, birthday);
    contactsDAO.save(contact);
    resp.sendRedirect("/contacts");
  }
}
