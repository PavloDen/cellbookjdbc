package edu.j4edu.servlets;

import edu.j4edu.dao.ContactsDAO;
import edu.j4edu.model.Contact;
import edu.j4edu.util.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/contacts")
public class ContactsServlet extends HttpServlet {
  private ContactsDAO contactsDAO;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    contactsDAO = DBConnection.createConnection();

    List<Contact> contacts = contactsDAO.findAll();
    req.setAttribute("contactsFromServer", contacts);
    req.getServletContext().getRequestDispatcher("/jsp/contacts.jsp").forward(req, resp);
  }
}
