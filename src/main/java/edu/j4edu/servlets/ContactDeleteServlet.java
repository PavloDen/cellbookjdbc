package edu.j4edu.servlets;

import edu.j4edu.dao.ContactsDAO;
import edu.j4edu.util.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/contact/delete")
public class ContactDeleteServlet extends HttpServlet {
  private ContactsDAO contactsDAO;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    contactsDAO = DBConnection.createConnection();
    Integer id = Integer.parseInt(request.getParameter("id"));
    contactsDAO.deleteById(id);
    response.sendRedirect("/contacts");
  }
}
