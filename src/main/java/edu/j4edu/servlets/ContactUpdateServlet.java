package edu.j4edu.servlets;

import edu.j4edu.dao.ContactsDAO;
import edu.j4edu.model.Contact;
import edu.j4edu.util.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;

@WebServlet("/contact/update")
public class ContactUpdateServlet extends HttpServlet {
  private ContactsDAO contactsDAO;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    contactsDAO = DBConnection.createConnection();
    Integer id = Integer.parseInt(request.getParameter("id"));
    Contact oldContact = contactsDAO.findById(id).orElseThrow(NoSuchElementException::new);
    request.setAttribute("contactFromServer", oldContact);

    request
        .getServletContext()
        .getRequestDispatcher("/jsp/ContactUpdateForm.jsp")
        .forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    int id = Integer.parseInt(req.getParameter("contact-id"));
    String fName = req.getParameter("first-name");
    String lName = req.getParameter("last-name");
    String note = req.getParameter("note");
    Date birthday = null;
    try {
      birthday = new SimpleDateFormat("yyy-MM-dd").parse(req.getParameter("birthday"));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    Contact contact = new Contact(id, fName, lName, note, birthday);
    contactsDAO.update(contact);
    resp.sendRedirect("/contacts");
  }
}
