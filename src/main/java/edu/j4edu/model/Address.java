package edu.j4edu.model;

import java.io.Serializable;

public class Address implements Serializable {
  private Integer addressId;
  private String firstLine;
  private String secondLine;
  private String zipCode;
  private String city;
  private String country;
  private Contact owner;

    public Address() {
    }


}
