package edu.j4edu.model;

import java.io.Serializable;

public class Phone implements Serializable {
  private Integer phoneId;
  private String phoneNumber;
  private String type;
  private Contact owner;

  public Phone() {}
}
