package edu.j4edu.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Contact implements Serializable {
  private Integer contactId;
  private String firstName;
  private String lastName;
  private String note;
  private Date birthday;
  private List<Phone> phones;
  private List<Address> addresses;
  private List<Email> emails;

  public Contact() {}

  public Contact(String firstName, String lastName, String note, Date birthday) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.note = note;
    this.birthday = birthday;
  }

  public Contact(Integer contactId, String firstName, String lastName, String note, Date birthday) {
    this.contactId = contactId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.note = note;
    this.birthday = birthday;
  }

  public Integer getContactId() {
    return contactId;
  }

  public void setContactId(Integer contactId) {
    this.contactId = contactId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public List<Phone> getPhones() {
    return phones;
  }

  public void setPhones(List<Phone> phones) {
    this.phones = phones;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

  public List<Email> getEmails() {
    return emails;
  }

  public void setEmails(List<Email> emails) {
    this.emails = emails;
  }
}
