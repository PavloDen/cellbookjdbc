package edu.j4edu.model;

import java.io.Serializable;

public class Email implements Serializable {
  private Integer emailId;
  private String address;
  private String type;
  private Contact owner;

  public Email() {}
}
